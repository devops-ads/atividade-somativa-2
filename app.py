from flask import Flask
from flask import request

app = Flask(__name__)

@app.route("/")
def index():
    numero = request.args.get("numero", "")
    if numero:
        dobro = dobro_de(numero)
    else:
        dobro = ""

    return (
        	"""<h2> Digite um número para saber o dobro </h2>"""
		"""<br>"""
		"""<form action="" method="get">
                <input type="text" name="numero">
                <input type="submit" value="Dobrar">
            </form>"""
        """<br>"""
        + "O numero digitado foi:  "
        + '<a id="numero">' +numero+ '</a>'
        """<br>"""
        """<br>"""
        """<br>"""
        + "O dobro do número digitado é:  "
        + '<a id="dobro">' +dobro+ '</a>'

    )
 
@app.route("/<int:numero>")
def dobro_de(numero):
    numero = numero
    dobro = float(numero) * 2
    dobro = round(dobro, 1) 
    return str(dobro)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080, debug=True)